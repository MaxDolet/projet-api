package miage.m2.sid.boundary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import miage.m2.sid.entities.ValeurChange;

@RestController
public class BourseRessource {

    @Autowired
    private Environment environment;

    @Autowired
    private ValeurChangeRepository vcr;

    @GetMapping("/change-devise/source/{source}/cible/{cible}")
    public ValeurChange getValeurDeChange(@PathVariable String source, @PathVariable String cible) {

        ValeurChange valeurChange = vcr.findBySourceAndCible(source, cible);
        valeurChange.setPort(Integer.parseInt(environment.getProperty("local.server.port")));

        return valeurChange;
    }
}
