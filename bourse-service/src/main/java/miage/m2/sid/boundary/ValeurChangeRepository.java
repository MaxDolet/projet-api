package miage.m2.sid.boundary;

import miage.m2.sid.entities.ValeurChange;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ValeurChangeRepository extends JpaRepository<ValeurChange, Long> {

    ValeurChange findBySourceAndCible(String source, String cible);
    
}
