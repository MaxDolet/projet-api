package miage.m2.sid.boundary;

import java.math.BigDecimal;
import miage.m2.sid.entities.DeviseConversionBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeviseConversionRessource {

    private ChangeServiceProxy proxy;
    
    @Autowired
    public DeviseConversionRessource(ChangeServiceProxy changeServiceProxy)
	{
		this.proxy = changeServiceProxy;
	}

    @GetMapping("/conversion-devise-feign/source/{source}/cible/{cible}/quantite/{qte}")
    public DeviseConversionBean conversionFeign(@PathVariable String source,
            @PathVariable String cible,
            @PathVariable BigDecimal qte) {

        DeviseConversionBean response = proxy.getValeurChange(source, cible);

        return new DeviseConversionBean(response.getId(), source, cible, response.getTauxConversion(), qte,
                qte.multiply(response.getTauxConversion()), response.getPort());
    }

}
