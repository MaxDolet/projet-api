package miage.m2.sid.boundary;

import miage.m2.sid.entities.DeviseConversionBean;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="bourse-service")
@RibbonClient(name="bourse-service")
public interface ChangeServiceProxy
{
    
  @GetMapping("/change-devise/source/{source}/cible/{cible}")
  DeviseConversionBean getValeurChange (@PathVariable("source") String source,
										@PathVariable("cible") String cible);

}